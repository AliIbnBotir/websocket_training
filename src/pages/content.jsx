import React, { useEffect, useState } from "react";
import { Progress } from "antd";
import { Button, notification } from "antd";

export default function Content() {
  let socket = new WebSocket("wss://ws.car24.uz/ws-random-2");

  const [progress, setProgress] = useState(0);
  const [message, setMessage] = useState("");
  const openNotification = (message, placement) => {
    notification.open({
      message: message,
      placement,
    });
  };
  let responseMessage = [];
  useEffect(() => {
    const timer = setInterval(() => {
      if (socket.bufferedAmount === 0) {
        socket.onmessage = function (event) {
          let message = JSON.parse(event.data);
          if (message.type === "progress") {
            if (progress >= 100) {
              setProgress(0);
            } else {
              setProgress((prev) => prev + message.data.count);
            }
          } else if (message.type === "notification") {
            openNotification(message.data.description, "topRight");
          } else if (message.type === "message") {
            let m = message.data.description;
            responseMessage.push(m);
            responseMessage.toString();
            setMessage(responseMessage);
          }
        };
      }
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, [progress]);
  return (
    <div className="contnent">
      <Progress percent={progress} />
      <div>
        <p>{message}</p>
      </div>
    </div>
  );
}
